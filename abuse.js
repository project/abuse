$(function(){
	// setup each ticket for inline/ajax moderation
	$('li.ticket').each(function(){
		var ticket = this;
		$ticket = $(this);
		var options = {
			target:	'#message-wrapper',
			url:	Drupal.base_url + 'admin/abuse/moderate/content/js',
			type:	'post',
			dataType:	'json',
			success:	function(data) {
				$('#message-wrapper').text(data.data);
				if (data.status == true) {
					$(ticket).hide('slow', function(){
						$(ticket).remove();
					});
				}
			}
		};
		$("form", $ticket).ajaxForm(options);
	});
});