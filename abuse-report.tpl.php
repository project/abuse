<?php
?>
<li class="ticket">
	<h2 class="title"><?php print l($object->title, $object->link); ?></h2>
	<dl class="info">
		<dt><?php print t('By:') ?></dt>
		<dd>
			<?php print theme('username', $account);?>
			<?php print t('( %offence offences | %warning warnings )', array('%offence' => $offences, '%warning' => $warnings)); ?>
			<?php print l(t("abuse history", array('@user' => $account->name)), 'admin/abuse/status/user/'. $account->uid); ?>
		</dd>
		<dt><?php print t('Type:'); ?></dt>
		<dd><?php print $nodeType; ?></dd>
		<dt><?php print t('Status:'); ?></dt>
		<dd><?php print $object->abuse_status_string; ?></dd>
		<?php if(variable_get('abuse_assigned_moderators', FALSE)): ?>
		<dt><?php print t('Assigned To:'); ?></dt>
		<dd><?php print $object->abuse_assigned_to_name; ?></dd>
		<?php endif; ?>
	</dl>
	<p class="buttons">
	  <?php print $moderate; ?>
	</p>
	<div class="text">          
		<?php print $object->content; ?>
		<dl class="script">
			<dt>Description:</dt>
			<dd><?php print $object->description; ?></dd>
		</dl>
	</div>
	<div class="history">
		<dl>
	      <dt><?php print t('History'); ?></dt>
	      <?php foreach($object->history as $log): ?>
	      <dd>
	        <strong><?php print $log->flagger; ?>:</strong> 
	        <?php print t('Changed status to %status', array('%status' => $log->readable_status)); ?>
	      </dd>
	      <?php endforeach; ?>
	    </dl>
		<dl>
	      <dt><?php print t('Warnings'); ?></dt>
	      <?php foreach($object->warnings as $warning): ?>
	      <dd>
	        <strong><?php print $warning->name; ?>:</strong> 
	        <?php print t('sent warning on %date', array('%date' => $warning->date)); ?>
	      </dd>
	      <?php endforeach; ?>
	    </dl>
		<dl>
		  <dt><?php print t('Flags'); ?></dt>
		  <?php foreach($object->reports as $report): ?>
		  <dd>
	        <strong><?php print theme("username", $report) . ' '. format_date($report->created); ?>:</strong> 
	        <?php print check_plain(urldecode($report->body)); ?>
	      </dd>
		  <?php endforeach; ?>
		</dl>
    </div>
</li>